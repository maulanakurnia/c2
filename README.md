# Assigment C2 - Intern Privy

Author : Maulana Kurnia Fiqih Ainul Yaqin

Position : Intern Backend

This repository contains C2 assignment that include:

1. [Golang-HTML](https://gitlab.com/maulanakurnia/c2/-/tree/main/golang-html)
2. [Golang-React](https://gitlab.com/maulanakurnia/c2/-/tree/main/golang-react)
