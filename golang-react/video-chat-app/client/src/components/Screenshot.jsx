import React, {useRef} from "react"
import html2canvas from "html2canvas";

// function Screenshot() {
//     const linkRef = useRef()
//
//     const captureScreen = async () => {
//         try {
//
//             const video = document.createElement("video")
//             const stream = await navigator.mediaDevices.getDisplayMedia();
//
//             video.addEventListener("loadedmetadata", () => {
//                 const canvas = document.createElement("canvas")
//                 const ctx = canvas.getContext("2d")
//
//                 canvas.width = video.videoWidth
//                 canvas.height = video.videoHeight
//
//                 video.play()
//                 ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
//                 stream.getVideoTracks()[0].stop();
//
//                 linkRef.current.href = canvas.toDataURL()
//                 linkRef.current.click()
//
//                 setTimeout(()=> {
//                     window.location.assign(imgRef.current.src)
//                 }, 2000)
//             });
//             video.srcObject = stream
//         } catch (error) {
//             alert("gagal mengambil screenshot!")
//         }
//     }
//
//     return(
//         <div>
//             <button onClick={captureScreen}>take screenshot</button>
//             <a download="screenshot.png" ref={linkRef} href="#" hidden={true}>downlaod image</a>
//         </div>
//     )
// }

function Screenshot() {
    const capture = () => {
        html2canvas(document.body).then(canvas => {
            const a = document.createElement('a')
            a.href = canvas.toDataURL("...assets/image/jpg").replace("image/png", "image/octet-system")
            a.download = `${Date.now()}.png`
            a.click()
        })
    }

    return (
        <div>
            <button onClick={capture}>take screenshot</button>
        </div>
    )
}

export default Screenshot