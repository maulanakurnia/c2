import React, {useEffect} from "react"
import { useRef, useState } from "react"

function ScreenRecord() {
    const videoRef = useRef()
    const btnStartRef = useRef()
    const btnStopRef = useRef()
    const [disabled, setDisabled] = useState(true)
    const [mediaRecorder, setMediaRecorder] = useState({stream: null})

    const startRecording = async () => {
        let displayMedia
        setDisabled(false)
        try {
            displayMedia = await navigator.mediaDevices.getDisplayMedia({
                video: true,
                audio: true
            })

            const recorder = new MediaRecorder(displayMedia)
            setMediaRecorder(recorder)

            const chunks = []
            recorder.ondataavailable = e => chunks.push(e.data)
            recorder.onstop = () => {
                const completeBlob = new Blob(chunks, {
                    type: "video/webm"
                })
                videoRef.current.src = URL.createObjectURL(completeBlob)
            }
            recorder.start()

        } catch (err) {
            setDisabled(true)
        }
    }

    const stopRecording = () => {
        if (mediaRecorder.stream.active) {
            mediaRecorder.stop()
        }

        mediaRecorder.stream.getTracks().map((track) => {
            track.stop()
        })

        setMediaRecorder({...mediaRecorder})

        setDisabled(true)

        setTimeout(() => {
            let clickedOK = confirm("want download record?")

            if (clickedOK) {
                window.open(videoRef.current.src)
            }
        }, 1000)
    }

    const handleStart = e => {
        e.preventDefault()
        startRecording()
    }

    const handleStop = e => {
        e.preventDefault()
        stopRecording()
    }

    useEffect(()=> {
        if(mediaRecorder.stream) {
            mediaRecorder.stream.getTracks()[0].onended = () => {
                stopRecording()
            }
        }
    })

    return (
        <div>
            <video  ref={videoRef} hidden />
            <button ref={btnStartRef} onClick={(e) => handleStart(e)} disabled={!disabled}>Start Recording</button>
            <button ref={btnStopRef} onClick={(e) => handleStop(e)} disabled={disabled}>Stop Recording</button>
        </div>

    );
}

export default ScreenRecord
