# Golang-React (Video Chat App)

## Screenshot

### Website

![web](screenshot/web.png)

### Choose Screen

![web](screenshot/choose-screen.png)

### Result Screen Record

![web](screenshot/result-record.png)

### Result Screenshot

![web](screenshot/screenshot.png)
