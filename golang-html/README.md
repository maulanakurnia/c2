# Golang-HTML (Video Chat App)

## Screenshot

### Website

![web](screenshot/web.png)

### Choose Screen

![web](screenshot/screenrecord-choose-screen.png)

### Result Screen Record

![web](screenshot/result-screen-record.png)

### Result Screenshot

![web](screenshot/screenshot.png)
